package com.custom.annoations;

public class JsonSerializationException extends Exception {
    JsonSerializationException(String msg) {
        super(msg);
    }
}
